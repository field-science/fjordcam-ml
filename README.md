# Attempt 1 at a Fjord camera trap classifier

## General process and what needs to be done
1. Build a dataset. This involved watching videos and manually labelling frames as `FISH` or `NO_FISH`. We could do something more complex than a binary classifier eventually, but for it would be a huge step just to have these two labels.

2. Train a model. The second step involves using our manually labelled data from step one to create a working model. This can be tricky, since there are a lot of settings that we can mess with to improve accuracy. 

3. Write code that can use that model to classify a video. We should be able to run future videos through our code to check whether there are fish, and it should be able to tell us which video file and at what time stamp things can be seen.


## Previous Work
I've used fast.ai before and it was pretty easy to work with. I'll start from there.
Making a binary classifier is the easiest place to start, so I'll start with `fish` and `no-fish` as my labels.

## Building a Dataset
### Getting images from video
For training I need to extract and label a set of images from the viedos. I can use FFMPEG to downsample and extract at a lower-than-video frame rate, that way I won't get 46,000 images to label for each video I run though it (25 minute video at 29.9FPS).
It seems (to me) like a sample rate of less than a second could result in overfitting anyway.

FFMPEG commands: 
```bash
ffmpeg -i sample_1.MP4 -vf fps=1 %04d.png              
# this should capture one frame for every second of video. 
```
This command results in an increase in data volume from the ~11GB video to ~15GB of images.
The program seems to limit by CPU, but it is parallel, so a machine with lots of cores like Hamilton should make short work of it. 

### Sorting images
At my scale, the easiest way to do this is to watch the video, and label some obvious sections (I'll start this below). 
This also solves the problem of some of the images being grainy or blurry, but still having fish in frame. It's really hard to tell from the still if you are looking at a fish or a smudge.

### Sample_1.mp4
| Video Segment | Label | Image Frames |
| - | - | - |
| 0:00-1:15 | no_fish | 0001-0076 |
| 3:00-3:30 | no_fish | 0181-0211 |
| 7:00-7:20 | no_fish | 0421-0441 |
| 7:21-7:23 | fish | 0442-0444 |
| 7:26-7:53 | fish | 0447-0474 |
| 7:54-8:15 | no_fish | 0475-0496 |
| 10:00-10:30 | no_fish | 0601-0631 |
| 12:00-12:30 | no_fish | 0721-0751 |
| 18:02-18:14 | fish | 1083-1095 |
| 20:05-20:11 | fish | 1206-1212 |
| 20:30-21:00 | no_fish | 1231-1261 |
| 21:22-22:09 | fish | 1283-1330 |

## Building a Model
### Environment
```
  Python            3.10.12
  fastai2           0.0.30
  fastapi           0.96.0
  fastbook          0.0.29
  fastcore          1.5.29
  fastdownload      0.0.7
  fastjsonschema    2.16.3
  fastprogress      1.0.3
```

You should be able to set this up with the following commands (double check your versions after)
1. `pip install fastai2==0.0.30`
2. `pip install fastbook==0.0.29`

## Current files notes
- `sample_1_1s` -  all images from sample_1.MP4, sampled at 1FPS.
  - `ffmpeg -i sample_1.MP4 -vf fps=1 %04d.png`
  - 1539 images, ~15GB

- `train_set_1` - first attempt at a training set based on `sample_1_1s`.
  - `scp -r USERNAME@cluster.earlham.edu:/eccs/home/pelibby16/projects/fjordcam-ml/train_set_1 train_set_1`
  - 100 "FISH" images
  - 244 "NO_FISH" images
