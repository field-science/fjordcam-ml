#!/usr/bin/env python
import fastbook
import argparse

from fastai2 import *
from fastai2.vision import *
from fastai.test_utils import *
from fastbook import *

fastbook.setup_book()

def create_model(input_dir, cycles, size):
    model = models.resnet34

    # Setup for fastai data
    class DataLoaders(GetAttr):
        def __init__(self, *loaders): self.loaders = loaders
        def __getitem__(self, i): return self.loaders[i]
        train,valid = add_props(lambda i,self: self[i])

    block = DataBlock(
        blocks=(ImageBlock, CategoryBlock), 
        get_items=get_image_files, 
        splitter=RandomSplitter(valid_pct=0.2, seed=42),
        get_y=parent_label,
        item_tfms=Resize(size)
    )

    # Create CNN Learner
    dls = block.dataloaders(input_dir)
    learn = cnn_learner(dls, model, metrics=error_rate)

    # Create fig 1 - SAMPLE BATCH
    #dls.show_batch(nrows=4, ncols=3, show=True)

    # Create fig 2 - learning rate
    #learn.lr_find()

    # TRAIN MODEL
    learn.fit_one_cycle(int(cycles))

    # Create fig 3 - plot loss
    learn.recorder.plot_loss()

    # Create Fig 4 - Confusion Matrix
    interp = ClassificationInterpretation.from_learner(learn)
    interp.plot_confusion_matrix()

    # Save model
    learn.export('outputs/model.pkl')


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Build a PKL model.')
    parser.add_argument('-i', '--input', type=str, help='Input directory with labelled sub-dirs', required=True)
    parser.add_argument('-c', '--cycles', type=int, help='Number of cycles to run', required=True)
    parser.add_argument('-s', '--size', type=int, help='Image size for resize transform', required=True)
    args = parser.parse_args()

    print("Input Dir: " + args.input + ", Output Dir: outputs/")
    print("Training cycles: " + str(args.cycles))
    print("Image size: " + str(args.size) + "x" + str(args.size))

    create_model(args.input, args.cycles, args.size)

